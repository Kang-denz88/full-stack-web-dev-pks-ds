<?php 
abstract class Hewan {
    public $nama;
    public function __construct($nama) {
        $this->nama = $nama;
    }

    abstract public function intro();
}

abstract class Fight {

}

class Hariamu extends Hewan {
    public function intro() {

    } 
}

class Elang extends Hewan {
    public function intro() {
        
    } 
}

class Harimau extends Fight {

}

class Elang extends Fight {

}
?>